IMPORTANT NOTES
===============

1. The project fully implements the cake list user story by showing collection of cakes.
2. iOS 9.0 onwards supported. Tested on iPhone 5 onwards devices in both orientation. 
3. The project has been developed on latest xcode 8.3.3 with Swift 3.1 without any warnings, errors or crashes.
4. The app is following MVVM-C (Model-View-ViewModel-Coordinator) architecture pattern. 
5. I preferred to use all the built-in native iOS technologies and not using third-party libraries or CocoaPods for this small project. Otherwise libraries like Alamofire, SwiftyJSON, SDWebImage and RxSwift could be useful.
7. Please follow xcode console for any user info messages incase of issue with retriving data. Also, no retry option is provided to the user to request reloading of data. Please re-run the app if this happens.
8. This project is built on best software engineering practices. To name a few: SOLID principles, composition over inheritance, protocol oriented programming and loosely coupled architecture.

Thanks for your time.
