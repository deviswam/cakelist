//
//  CakeListViewController.swift
//  CakeList
//
//  Created by Waheed Malik on 16/07/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class CakeListViewController: UIViewController {

    @IBOutlet weak var cakeListTableView: UITableView!
    
    var cakeListTableViewDataSource: UITableViewDataSource?
    var viewModel: CakeListViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cakeListTableView.dataSource = cakeListTableViewDataSource
        
        // load cakes
        viewModel?.getCakes()
    }
}

extension CakeListViewController: CakeListViewModelViewDelegate {
    func cakesLoaded(with result: Result<Void>) {
        switch result {
        case .success:
            self.cakeListTableView.reloadData()
        case .failure(let error):
            print("Info (Inform user): Unable to fetch cakes list. Internal error: \(error.localizedDescription)")
        }
    }
}
