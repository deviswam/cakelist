//
//  CakeTableViewCell.swift
//  CakeList
//
//  Created by Waheed Malik on 16/07/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

protocol CakeTableViewCell {
    func configCell(with cakeViewModel: CakeViewModel)
    func setCakeImage(image: UIImage)
}

class CLCakeTableViewCell: UITableViewCell, CakeTableViewCell {
    
    @IBOutlet weak var cakeTitleLabel: UILabel!
    @IBOutlet weak var cakeDescriptionLabel: UILabel!
    @IBOutlet weak var cakeImageView: UIImageView!
    
    
    func configCell(with cakeViewModel: CakeViewModel) {
        cakeTitleLabel.text = cakeViewModel.title
        cakeDescriptionLabel.text = cakeViewModel.description
    }
    
    func setCakeImage(image: UIImage) {
        self.cakeImageView.image = image
    }
    
    override func prepareForReuse() {
        self.cakeImageView.image = UIImage(named: "cake_placeholder")
    }
}

