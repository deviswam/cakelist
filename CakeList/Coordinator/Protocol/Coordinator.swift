//
//  Coordinator.swift
//  CakeList
//
//  Created by Waheed Malik on 16/07/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol Coordinator {
    func start()
}
