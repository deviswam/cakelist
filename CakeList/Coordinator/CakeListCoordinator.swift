//
//  CakeListCoordinator.swift
//  CakeList
//
//  Created by Waheed Malik on 16/07/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class CakeListCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    fileprivate var navigationController: UINavigationController
    
    // MARK:- INITIALIZER
    init(navController: UINavigationController) {
        self.navigationController = navController
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        if let cakeListVC = navigationController.topViewController as? CakeListViewController {
            let cakeApiClient = CLCakeAPIClient()
            let imageStore = CLImageStore(apiClient: cakeApiClient)
            let cakeManager = CLCakeManager(apiClient: cakeApiClient, imageStore: imageStore)
            let cakeListTableViewDataSource = CakesTableViewDataSource()
            let cakeListViewModel = CLCakeListViewModel(cakeManager: cakeManager, viewDelegate: cakeListVC)
            
            cakeListVC.viewModel = cakeListViewModel
            cakeListVC.cakeListTableViewDataSource = cakeListTableViewDataSource
            cakeListTableViewDataSource.viewModel = cakeListViewModel
        }
    }
}
