//
//  AppCoordinator.swift
//  CakeList
//
//  Created by Waheed Malik on 16/07/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class AppCoordinator : Coordinator {
    // MARK: PRIVATE VARIABLES
    private var window: UIWindow
    private var cakeListCoordinator : CakeListCoordinator?
    
    // MARK: INITIALIZER
    init(window: UIWindow) {
        self.window = window
    }
    
    // MARK: PUBLIC METHODS
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let rootNavController = storyboard.instantiateInitialViewController() as? UINavigationController {
            self.window.rootViewController = rootNavController
            self.showCakeList(rootNavController: rootNavController)
        }
    }
    
    // MARK: PRIVATE METHODS
    private func showCakeList(rootNavController: UINavigationController) {
        cakeListCoordinator = CakeListCoordinator(navController: rootNavController)
        cakeListCoordinator?.start()
    }
}
