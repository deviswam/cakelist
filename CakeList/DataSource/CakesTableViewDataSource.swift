//
//  CakesTableViewDataSource.swift
//  CakeList
//
//  Created by Waheed Malik on 16/07/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class CakesTableViewDataSource: NSObject, UITableViewDataSource {
    var viewModel: CakeListViewModel?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let viewModel = viewModel {
            return viewModel.numberOfCakes()
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CakeTableViewCell", for: indexPath)
        if let cakeCell = cell as? CakeTableViewCell,
            let cakeVM = viewModel?.cake(at: indexPath.row) {
            cakeCell.configCell(with: cakeVM)
            self.loadPhoto(at: indexPath, onCell: cakeCell)
        }
        return cell
    }
    
    private func loadPhoto(at indexPath: IndexPath, onCell cell: CakeTableViewCell) {
        viewModel?.photo(at: indexPath.item, completionHandler: { (result: Result<UIImage>) in
            if case let Result.success(image) = result {
                cell.setCakeImage(image: image)
            }
        })
    }
}
