//
//  ImageStore.swift
//  CakeList
//
//  Created by Waheed Malik on 15/07/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

// This class retrived the photo if it is already cached,
// otherwise downloads and then caches the image.
protocol ImageStore {
    func getImage(with imageUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void)
}

class CLImageStore: ImageStore {
    private let imageCache = NSCache<NSString, UIImage>()
    private let apiClient: CakeAPIClient
    
    init(apiClient: CakeAPIClient) {
        self.apiClient = apiClient
    }
    
    func getImage(with imageUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        let nsImageUrl = imageUrl as NSString
        if let image = imageCache.object(forKey: nsImageUrl) {
            completionHandler(.success(image))
        } else {
            apiClient.fetchCakePhoto(with: imageUrl, { [weak self] (result: Result<UIImage>) in
                switch result {
                case .success(let image):
                    self?.imageCache.setObject(image, forKey: nsImageUrl)
                case .failure(let error):
                    print("Unable to download image. Internal error: \(error)")
                }
                completionHandler(result)
            })
        }
    }
}
