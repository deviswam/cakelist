//
//  Cake.swift
//  CakeList
//
//  Created by Waheed Malik on 15/07/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol Cake {
    var title: String { get }
    var description: String { get }
    var imageURLStr: String { get }
}

struct CLCake: Cake {
    var title: String
    var description: String
    var imageURLStr: String
    
    init(title: String, description: String, imageURLStr: String) {
        self.title = title
        self.description = description
        self.imageURLStr = imageURLStr
    }
    
    init?(dictionary: Dictionary<String, Any>?) {
        guard let dictionary = dictionary,
            let title = dictionary["title"] as? String,
            let desc = dictionary["desc"] as? String,
            let imageURLStr = dictionary["image"] as? String else {
                return nil
        }
        self.init(title: title, description: desc, imageURLStr: imageURLStr)
    }
}
