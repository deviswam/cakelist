//
//  CakeAPIClient.swift
//  CakeList
//
//  Created by Waheed Malik on 15/07/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

enum CakeAPIClientError: Error {
    case httpError
    case invalidDataError
    case dataSerializationError
}

protocol CakeAPIClient {
    func fetchCakes(completionHandler: @escaping (_ result: Result<[Cake]>) -> Void)
    func fetchCakePhoto(with photoUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void)
}

class CLCakeAPIClient: CakeAPIClient {
    // MARK: PRIVATE VARIABLES
    private let CAKES_URL = "https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json"
    private let urlSession: URLSession!
    
    // MARK: INITIALIZER
    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    // MARK: PUBLIC METHODS
    func fetchCakes(completionHandler: @escaping (_ result: Result<[Cake]>) -> Void) {
        guard let urlRequest = self.createURLRequest(from: CAKES_URL) else {
            return
        }
        self.getCakes(with: urlRequest) { (result: Result<[Cake]>) in
            completionHandler(result)
        }
    }
    
    func fetchCakePhoto(with photoUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        //print("WAM PHOTO URL:\(url.absoluteString)")
        guard let urlRequest = self.createURLRequest(from: photoUrl) else {
            return
        }
        self.getPhoto(with: urlRequest) { (result: Result<UIImage>) in
            completionHandler(result)
        }
    }
    
    // MARK: PRIVATE METHODS
    private func createURLRequest(from urlString:String, with parameters:[String: String] = [:]) -> URLRequest? {
        // create url
        var urlComponents = URLComponents(string: urlString)
        
        // append custom parameters
        var queryItems = [URLQueryItem]()
        for (key, value) in parameters {
            let query = URLQueryItem(name: key, value: value)
            queryItems.append(query)
        }
        urlComponents?.queryItems = queryItems
        guard let url = urlComponents?.url else { return nil }
        //print("WAM URL:\(url.absoluteString)")
        
        // create request object
        let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15)
        
        return request
    }
    
    private func getCakes(with urlRequest: URLRequest, completionHandler: @escaping (_ result: Result<[Cake]>) -> Void) {
        let dataTask = urlSession.dataTask(with: urlRequest) { [unowned self] (data, urlResponse, error) in
            var result: Result<[Cake]>!
            
            if error != nil {
                result = .failure(CakeAPIClientError.httpError)
            } else if let data = data {
                do {
                    if let cakesArray = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: AnyObject]] {
                        result = .success(self.createCakes(from: cakesArray))
                    }
                } catch {
                    result = .failure(CakeAPIClientError.dataSerializationError)
                }
            }
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
        dataTask.resume()
    }
    
    
    private func createCakes(from cakesArray:[[String : AnyObject]]) -> [Cake] {
        let cakes = cakesArray.map { (cakeDictionary: [String : AnyObject]) -> Cake? in
            return CLCake(dictionary: cakeDictionary)
            }.filter { (cake: Cake?) -> Bool in
                return cake != nil
            } as! [Cake]
        
        return cakes
    }
    
    private func getPhoto(with urlRequest: URLRequest, completionHandler: @escaping (_ result: Result<UIImage>) -> Void) {
        let dataTask = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
            var result: Result<UIImage>!
            if error != nil {
                result = .failure(CakeAPIClientError.httpError)
            } else if let data = data {
                let image = UIImage(data: data)
                if image == nil {
                    result = .failure(CakeAPIClientError.invalidDataError)
                } else {
                    result = .success(image!)
                }
            }
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
        dataTask.resume()
    }
}
