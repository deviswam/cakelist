//
//  CakeManager.swift
//  CakeList
//
//  Created by Waheed Malik on 15/07/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

enum CakeManagerError : LocalizedError {
    case noCakesFoundError
    case noPhotoFoundError
    case networkError
    
    var localizedDescription: String {
        switch self {
        case .noCakesFoundError:
            return "No Cakes Found"
        case .noPhotoFoundError:
            return "No Photo Found"
        case .networkError:
            return "Network Error"
        }
    }
}

//MARK: INTERNAL ENUM
enum CakeManagerMethodType {
    case cakes
    case photo
}

protocol CakeManager {
    func loadCakes(completionHandler: @escaping (_ result: Result<[Cake]>) -> Void)
    func loadPhoto(with photoUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void)
}

class CLCakeManager: CakeManager {
    // MARK: PRIVATE VARIABLES
    private let apiClient: CakeAPIClient
    private let imageStore: ImageStore
    
    // MARK: INITIALIZER
    init(apiClient: CakeAPIClient, imageStore: ImageStore) {
        self.apiClient = apiClient
        self.imageStore = imageStore
    }
    
    // MARK: PUBLIC METHODS
    func loadCakes(completionHandler: @escaping (Result<[Cake]>) -> Void) {
        apiClient.fetchCakes { [unowned self] (result: Result<[Cake]>) in
            var mResult = result
            if case let Result.failure(error) = result {
                mResult = .failure(self.cakeManagerError(from: error as! CakeAPIClientError, for: .cakes))
            }
            completionHandler(mResult)
        }
    }
    
    func loadPhoto(with photoUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        imageStore.getImage(with: photoUrl) { [unowned self] (result: Result<UIImage>) in
            var mResult = result
            if case let Result.failure(error) = result {
                mResult = .failure(self.cakeManagerError(from: error as! CakeAPIClientError, for: .photo))
            }
            completionHandler(mResult)
        }
    }
    
    // MARK: PRIVATE METHODS
    private func cakeManagerError(from apiError: CakeAPIClientError, for methodType: CakeManagerMethodType) -> CakeManagerError {
        switch apiError {
        case .dataSerializationError:
            return CakeManagerError.noCakesFoundError
        case .invalidDataError:
            switch methodType {
            case .cakes:
                return CakeManagerError.noCakesFoundError
            case .photo:
                return CakeManagerError.noPhotoFoundError
            }
        case .httpError:
            return CakeManagerError.networkError
        }
    }
}
