//
//  CakeViewModel.swift
//  CakeList
//
//  Created by Waheed Malik on 15/07/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol CakeViewModel {
    var title: String { get }
    var description: String { get }
}

class CLCakeViewModel: CakeViewModel {
    var title: String = ""
    var description: String = ""
    
    init(cake: Cake) {
        title = cake.title
        description = cake.description
    }
}
