//
//  CakeListViewModel.swift
//  CakeList
//
//  Created by Waheed Malik on 15/07/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

// MARK:- VIEW DELEGATE PROTOCOL
protocol CakeListViewModelViewDelegate: class {
    func cakesLoaded(with result: Result<Void>)
}

// MARK:- VIEWMODEL PROTOCOL
protocol CakeListViewModel {
    func getCakes()
    func photo(at index: Int, completionHandler: @escaping (Result<UIImage>) -> Void)
    
    func numberOfCakes() -> Int
    func cake(at index: Int) -> CakeViewModel?
}

class CLCakeListViewModel: CakeListViewModel {
    // MARK: PRIVATE VARIABLES
    private let cakeManager: CakeManager
    private weak var viewDelegate: CakeListViewModelViewDelegate!
    private var cakes: [Cake]?
    
    // MARK: INITIALIZER
    init(cakeManager: CakeManager, viewDelegate: CakeListViewModelViewDelegate) {
        self.cakeManager = cakeManager
        self.viewDelegate = viewDelegate
    }
    
    // MARK: PUBLIC METHODS
    func getCakes() {
        var vmResult: Result<Void>!
        cakeManager.loadCakes { [weak self] (result: Result<[Cake]>) in
            switch result {
            case .success(let cakes):
                self?.cakes = cakes
                vmResult = .success()
            case .failure(let error):
                vmResult = .failure(error)
            }
            self?.viewDelegate.cakesLoaded(with: vmResult)
        }
    }
    
    func numberOfCakes() -> Int {
        guard let cakes = self.cakes else {
            return 0
        }
        return cakes.count
    }
    
    func cake(at index: Int) -> CakeViewModel? {
        if let cakes = self.cakes, index < cakes.count {
            return CLCakeViewModel(cake: cakes[index])
        }
        return nil
    }
    
    func photo(at index: Int, completionHandler: @escaping (Result<UIImage>) -> Void) {
        if let cakes = self.cakes, index < cakes.count {
            let cakeImageUrl = cakes[index].imageURLStr
            cakeManager.loadPhoto(with: cakeImageUrl) { (result: Result<UIImage>) in
                completionHandler(result)
            }
        }
    }
}
