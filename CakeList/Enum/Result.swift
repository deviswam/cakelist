//
//  Result.swift
//  CakeList
//
//  Created by Waheed Malik on 15/07/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//
import Foundation

enum Result<T> {
    case success(T)
    case failure(Error)
}
